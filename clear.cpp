#include "Glossary.hpp"

void Glossary::clear()
{
    std::cout << "Clearing" << std::endl;
    for(auto el : dict)
    {
        delete el;
    }
    dict.clear();
    std::cout << dict.size() << std::endl;
}