#include "Glossary.hpp"

void Glossary::load()
{
    std::cout << "Loading from " << this->savepath << std::endl;
    std::ifstream fin(this->savepath, std::ios::binary);
    cereal::BinaryInputArchive iarchive(fin);
    if (fin.is_open())
    {
        iarchive(*this);    // Ïîëó÷àåì êîëè÷åñòâî ÿ÷ååê â çàãðóçêå
        add(this->word_counter);


        for (auto el : dict)
        {
            iarchive(*el);
        }
    }
    else
    {
        std::cout << "Error" << std::endl;
    }
    fin.close();
}