#include "Glossary.hpp"

void Glossary::save()
{
    std::cout << "Saving in " << this->savepath << std::endl;
    std::ofstream fout(this->savepath, std::ios::binary | std::ios::trunc);
    cereal::BinaryOutputArchive oarchive(fout);
    
    if (fout.is_open())
    {
        oarchive(*this);

        for (auto el : dict)
            oarchive(*el);
    }
    else
    {
        std::cout << "Error" << std::endl;
    }
    fout.close();
}