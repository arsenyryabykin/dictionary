#include "functions.hpp"

class Cell
{
	private:
		std::string word;					// Слово, словосочетания, идиома или фразовый гл-л
		std::string interpretation;
		std::vector<std::string> translations;		// Переводы на родной язык
		std::vector<std::string> context;
		std::vector<std::string> synonyms;


	public:
		Cell()
		{
			std::cout << "Cell was built" << std::endl;
		}
		~Cell()
		{
			std::cout << "Cell was destructed" << std::endl;
		}

		void add_word(const std::string&);		
        std::string get_word();
        
		void add_translation(const std::string&);
		std::vector<std::string> get_translations();
        
		void add_context(const std::string&);
		std::vector<std::string> get_context();
        
		void add_interpretation(const std::string& interpretation);
		std::string get_interpretation();

		void display(int) const;
        
        void save(std::filesystem::path);
		
		template <class Archive>
		void serialize(Archive& ar)
		{
			ar(word, interpretation, context);
		}
};

