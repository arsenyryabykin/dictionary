#include "Glossary.hpp"

void Glossary::add()
{
    Cell* new_cell = new Cell();
    dict.push_back(new_cell);
    word_counter++;

    std::cout << "Add Cell" << std::endl;
}

void Glossary::add(int cells_number)
{
    for (int i = 0; i < cells_number; i++)
    {
        dict.push_back(new Cell());
    }
}

void Glossary::add(Cell* cell)
{
    dict.push_back(cell);
    word_counter++;

    save();

    std::cout << "Add Cell" << std::endl;

}