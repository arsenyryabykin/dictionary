#include "Glossary.hpp"

void Cell::display(int cell_number) const
{
	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
	std::cout <<  "\t" << cell_number << "\t" << word << std::endl;
//	std::cout << "----------------------------------\n\n";


	
	std::cout << "Interpretation: " << std::endl;
	std::cout << "\t" << interpretation << std::endl;

//	std::cout << "Translations: " << std::endl;
//	for(auto el : translations)
//	{
//		std::cout << "\t" << el << std::endl;
//	}

	std::cout << "Context: " << std::endl;
	for(auto el : context)
	{
		std::cout << "\t" << el << std::endl;
	}
	std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
}

void Glossary::display_list() const
{
	cout << "Glossary: " << glossary_name << ", " << glossary_counter << endl;
    cout << "Number of words: " << dict.size() << endl;

    for(size_t i = 0; i < dict.size(); i++)
    {
        std::cout << "----------------------------------\n";
        std::cout << "\t" << i + 1 << "\t" << dict[i]->get_word() << std::endl;
        std::cout << "----------------------------------\n";
    }

}


void Glossary::display(int cell_index) const
{
    dict[cell_index - 1]->display(cell_index);
}