#include "Cell.hpp"


void Cell::add_word(const std::string& word)
{
	this->word = word;
}

std::string Cell::get_word()
{
	return word;
}

void Cell::add_translation(const std::string& translation)
{
	translations.push_back(translation);
}

std::vector<std::string> Cell::get_translations()
{
	return translations;
}


void Cell::add_context(const std::string& cont)
{
	context.push_back(cont);
}

std::vector<std::string> Cell::get_context()
{
	return context;
}

void Cell::add_interpretation(const std::string& interpretation)
{
	this->interpretation = interpretation;
}

std::string Cell::get_interpretation()
{
	return interpretation;
}

