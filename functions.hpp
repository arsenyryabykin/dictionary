#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <array>
#include <fstream>
#include <filesystem>
#include <cereal/archives/binary.hpp>
#include "cereal/types/vector.hpp"
#include "cereal/types/string.hpp"

using namespace std;