#include "Cell.hpp"


class Glossary 
{
	
        vector<Cell*> dict;        // Вектор указателей на объекты Cell
        
        int word_counter;       // Счетчик кол-ва слов в конкретном глоссарии
        filesystem::path savepath;      // Путь к папке сохранения слов
        string glossary_name;           // Имя словаря

        static int glossary_counter;      // Порядковый номер словаря
    
	public:
        Glossary(const string glossary_name)
        {
            glossary_counter++;
            this->glossary_name = glossary_name;
            word_counter = 0;
            path_constructing();
            //std::cout << "Dictionary was constructed" << std::endl;
        }
        ~Glossary()
        {
            for(auto el : dict)
                delete el;

            //std::cout << "Dictionary was destroyed" << std::endl;
        }

        void display_list() const;
        void display(int) const;

        static int get_glossary_count()
        {
            return glossary_counter;
        }

        void add();
        void add(Cell*);
        void add(int);

        void del(int);
                
        void clear();

        void save();
        void load();

        int get_cells_number();

        template <class Archive>
        void serialize(Archive& ar)
        {
            ar(word_counter);
        }

        void path_constructing();
};
        
